
 <div class="sidebar">
      <div class="sidebar-header">
        <div>
          <a href="#0" class="sidebar-logo"><span>GestConge</span></a>
          <small class="sidebar-logo-headline">votre application de cong�</small>
        </div>
      </div><!-- sidebar-header -->
      <div id="dpSidebarBody" class="sidebar-body">
        <ul class="nav nav-sidebar">
          <li class="nav-label"><label class="content-label">Template Pages</label></li>
          <li class="nav-item show">
          
             <c:url value="/home/" var="home"/>
            <a href="${home}" class="nav-link active"><i data-feather="box"></i> <fmt:message key="common.dashboard" /></a>
           
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link with-sub"><i data-feather="layout"></i> App Pages</a>
            <nav class="nav nav-sub">
              <a href="app-calendar.html" class="nav-sub-link">Calendar</a>
              <a href="app-chat.html" class="nav-sub-link">Chat</a>
              <a href="app-mail.html" class="nav-sub-link">Mail</a>
            </nav>
          </li>
       
          <li class="nav-label"><label class="content-label">Administration</label></li>
         
          
          <li class="nav-item">
           <c:url value="/departement/" var="departement"/>
            <a href="${departement }" class="nav-link"><i data-feather="book"></i> <fmt:message key="common.departement" /></a>
           </li>
       
          
           <li class="nav-item">
           <c:url value="/diplome/" var="diplome"/>
            <a href="${diplome }" class="nav-link"><i data-feather="book"></i> <fmt:message key="common.diplome" /></a>
            
          </li>
          
           <li class="nav-item">
           
           <c:url value="/fonction/" var="fonction"/>
            <a href="${fonction }" class="nav-link"><i data-feather="book"></i><fmt:message key="common.fonction" /></a>
            <nav class="nav nav-sub">
              <a href="http://themepixels.me/cassie/components/con-grid.html" class="nav-sub-link">Grid System</a>
              <a href="http://themepixels.me/cassie/components/con-icons.html" class="nav-sub-link">Icons</a>
              <a href="http://themepixels.me/cassie/components/con-images.html" class="nav-sub-link">Images</a>
              <a href="http://themepixels.me/cassie/components/con-typography.html" class="nav-sub-link">Typography</a>
            </nav>
          </li>
          
          
           <li class="nav-item">
           
           <c:url value="/grade/" var="grade"/>
            <a href="${grade }" class="nav-link"><i data-feather="book"></i> <fmt:message key="common.grade" /></a>
          
          </li>
          
          <li class="nav-label"><label class="content-label">Personnel</label></li>
           <li class="nav-item">
            <c:url value="/enseignant/" var="enseignant"/>
            <a href="${enseignant }" class="nav-link "><i data-feather="book"></i> <fmt:message key="common.enseignant" /></a>
           
          </li>
          
           <li class="nav-label"><label class="content-label">Demande Cong�</label></li>
           <li class="nav-item">
           <c:url value="/conge/" var="conge"/>
            <a href="${conge }" class="nav-link "><i data-feather="book"></i><fmt:message key="common.conge" /></a>
           
          </li>
          
           <li class="nav-item">
           
           <c:url value="/absence/" var="absence"/> 
            <a href="${absence }" class="nav-link "><i data-feather="book"></i> <fmt:message key="common.absence" /></a>
           
          </li>
          
          <li class="nav-label"><label class="content-label"> Configuration</label></li>
           <li class="nav-item">
            <a href="#" class="nav-link with-sub"><i data-feather="user"></i> <fmt:message key="common.parametrage" /></a>
            <nav class="nav nav-sub">
            <c:url value="/utilisateur/" var="user"/> 
              <a href="${user }" class="nav-sub-link"><fmt:message key="common.parametrage.utilisateur" /></a>
              <a href="page-timeline.html" class="nav-sub-link">Timeline</a>
              <a href="page-people.html" class="nav-sub-link">People</a>
              <a href="page-settings.html" class="nav-sub-link">Profile Settings</a>
            </nav>
          </li>
          
          
          <li class="nav-item">
            <a href="#" class="nav-link with-sub"><i data-feather="layers"></i> Components</a>
            <nav class="nav nav-sub">
              <a href="http://themepixels.me/cassie/components/com-accordion.html" class="nav-sub-link">Accordion</a>
              <a href="http://themepixels.me/cassie/components/com-alerts.html" class="nav-sub-link">Alerts</a>
              <a href="http://themepixels.me/cassie/components/com-avatar.html" class="nav-sub-link">Avatar</a>
              <a href="http://themepixels.me/cassie/components/com-badge.html" class="nav-sub-link">Badge</a>
              <a href="http://themepixels.me/cassie/components/com-breadcrumbs.html" class="nav-sub-link">Breadcrumbs</a>
              <a href="http://themepixels.me/cassie/components/com-buttons.html" class="nav-sub-link">Buttons</a>
              <a href="http://themepixels.me/cassie/components/com-button-groups.html" class="nav-sub-link">Button Groups</a>
              <a href="http://themepixels.me/cassie/components/com-cards.html" class="nav-sub-link">Cards</a>
              <a href="http://themepixels.me/cassie/components/com-carousel.html" class="nav-sub-link">Carousel</a>
              <a href="http://themepixels.me/cassie/components/com-collapse.html" class="nav-sub-link">Collapse</a>
              <a href="http://themepixels.me/cassie/components/com-dropdown.html" class="nav-sub-link">Dropdown</a>
              <a href="http://themepixels.me/cassie/components/com-list-group.html" class="nav-sub-link">List Group</a>
              <a href="http://themepixels.me/cassie/components/com-marker.html" class="nav-sub-link">Marker</a>
              <a href="http://themepixels.me/cassie/components/com-media-object.html" class="nav-sub-link">Media Object</a>
              <a href="http://themepixels.me/cassie/components/com-modal.html" class="nav-sub-link">Modal</a>
              <a href="http://themepixels.me/cassie/components/com-navs.html" class="nav-sub-link">Navs</a>
              <a href="http://themepixels.me/cassie/components/com-navbar.html" class="nav-sub-link">Navbar</a>
              <a href="http://themepixels.me/cassie/components/com-off-canvas.html" class="nav-sub-link">Off-Canvas</a>
              <a href="http://themepixels.me/cassie/components/com-pagination.html" class="nav-sub-link">Pagination</a>
              <a href="http://themepixels.me/cassie/components/com-placeholder.html" class="nav-sub-link">Placeholder</a>
              <a href="http://themepixels.me/cassie/components/com-popovers.html" class="nav-sub-link">Popovers</a>
							<a href="http://themepixels.me/cassie/components/com-progress.html" class="nav-sub-link">Progress</a>
              <a href="http://themepixels.me/cassie/components/com-steps.html" class="nav-sub-link">Steps</a>
              <a href="http://themepixels.me/cassie/components/com-scrollbar.html" class="nav-sub-link">Scrollbar</a>
              <a href="http://themepixels.me/cassie/components/com-scrollspy.html" class="nav-sub-link">Scrollspy</a>
              <a href="http://themepixels.me/cassie/components/com-spinners.html" class="nav-sub-link">Spinners</a>
              <a href="http://themepixels.me/cassie/components/com-tab.html" class="nav-sub-link">Tab</a>
              <a href="http://themepixels.me/cassie/components/com-toast.html" class="nav-sub-link">Toast </a>
              <a href="http://themepixels.me/cassie/components/com-tooltips.html" class="nav-sub-link">Tooltips</a>
              <a href="http://themepixels.me/cassie/components/com-table-basic.html" class="nav-sub-link">Table Basic</a>
              <a href="http://themepixels.me/cassie/components/com-table-advanced.html" class="nav-sub-link">Table (Datatable)</a>
            </nav>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link with-sub"><i data-feather="monitor"></i> Utilities</a>
            <nav class="nav nav-sub">
              <a href="http://themepixels.me/cassie/components/util-animation.html" class="nav-sub-link">Animation</a>
              <a href="http://themepixels.me/cassie/components/util-background.html" class="nav-sub-link">Background</a>
              <a href="http://themepixels.me/cassie/components/util-border.html" class="nav-sub-link">Border</a>
              <a href="http://themepixels.me/cassie/components/util-display.html" class="nav-sub-link">Display</a>
              <a href="http://themepixels.me/cassie/components/util-divider.html" class="nav-sub-link">Divider</a>
              <a href="http://themepixels.me/cassie/components/util-flex.html" class="nav-sub-link">Flex</a>
              <a href="http://themepixels.me/cassie/components/util-height.html" class="nav-sub-link">Height</a>
              <a href="http://themepixels.me/cassie/components/util-margin.html" class="nav-sub-link">Margin</a>
              <a href="http://themepixels.me/cassie/components/util-padding.html" class="nav-sub-link">Padding</a>
              <a href="http://themepixels.me/cassie/components/util-position.html" class="nav-sub-link">Position</a>
              <a href="http://themepixels.me/cassie/components/util-typography.html" class="nav-sub-link">Typography</a>
              <a href="http://themepixels.me/cassie/components/util-width.html" class="nav-sub-link">Width</a>
              <a href="http://themepixels.me/cassie/components/util-extras.html" class="nav-sub-link">Extras</a>
            </nav>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link with-sub"><i data-feather="life-buoy"></i> Forms</a>
            <nav class="nav nav-sub">
              <a href="http://themepixels.me/cassie/components/form-elements.html" class="nav-sub-link">Form Elements</a>
              <a href="http://themepixels.me/cassie/components/form-input-group.html" class="nav-sub-link">Input Group</a>
              <a href="http://themepixels.me/cassie/components/form-input-tags.html" class="nav-sub-link">Input Tags</a>
              <a href="http://themepixels.me/cassie/components/form-input-masks.html" class="nav-sub-link">Input Masks</a>
              <a href="http://themepixels.me/cassie/components/form-validation.html" class="nav-sub-link">Form Validation</a>
              <a href="http://themepixels.me/cassie/components/form-wizard.html" class="nav-sub-link">Form Wizard</a>
              <a href="http://themepixels.me/cassie/components/form-text-editor.html" class="nav-sub-link">Text Editor</a>
              <a href="http://themepixels.me/cassie/components/form-rangeslider.html" class="nav-sub-link">Range Slider</a>
              <a href="http://themepixels.me/cassie/components/form-datepicker.html" class="nav-sub-link">Date Pickers</a>
              <a href="http://themepixels.me/cassie/components/form-select2.html" class="nav-sub-link">Select2</a>
              <a href="http://themepixels.me/cassie/components/form-search.html" class="nav-sub-link">Search</a>
            </nav>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link with-sub"><i data-feather="pie-chart"></i> Charts</a>
            <nav class="nav nav-sub">
              <a href="http://themepixels.me/cassie/components/chart-flot.html" class="nav-sub-link">Flot</a>
              <a href="http://themepixels.me/cassie/components/chart-chartjs.html" class="nav-sub-link">ChartJS</a>
              <a href="http://themepixels.me/cassie/components/chart-peity.html" class="nav-sub-link">Peity</a>
              <a href="http://themepixels.me/cassie/components/chart-sparkline.html" class="nav-sub-link">Sparkline</a>
              <a href="http://themepixels.me/cassie/components/chart-morris.html" class="nav-sub-link">Morris</a>
            </nav>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link with-sub"><i data-feather="map-pin"></i> Maps</a>
            <nav class="nav nav-sub">
              <a href="http://themepixels.me/cassie/components/map-google.html" class="nav-sub-link">Google Maps</a>
              <a href="http://themepixels.me/cassie/components/map-leaflet.html" class="nav-sub-link">Leaflet Maps</a>
              <a href="http://themepixels.me/cassie/components/map-vector.html" class="nav-sub-link">Vector Maps</a>
            </nav>
          </li>
        </ul>

        <hr class="mg-t-30 mg-b-25">

        <ul class="nav nav-sidebar">
          <li class="nav-item"><a href="themes.html" class="nav-link"><i data-feather="aperture"></i> Themes</a></li>
          <li class="nav-item"><a href="http://themepixels.me/cassie/docs.html" class="nav-link"><i data-feather="help-circle"></i> Documentation</a></li>
        </ul>


      </div><!-- sidebar-body -->
    </div><!-- sidebar -->
    
     <div class="content content-page">
      <div class="header">
        <div class="header-left">
          <a href="#" class="burger-menu"><i data-feather="menu"></i></a>

          <div class="header-search">
            <i data-feather="search"></i>
            <input type="search" class="form-control" placeholder="Que cherchez-vous?">
          </div><!-- header-search -->
        </div><!-- header-left -->