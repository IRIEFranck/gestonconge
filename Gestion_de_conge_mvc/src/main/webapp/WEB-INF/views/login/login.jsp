<%@ include file="/WEB-INF/views/includes/includes.jsp" %>
<!DOCTYPE html>
<html lang="en">
  
<!-- Mirrored from themepixels.me/cassie/pages/page-signin.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jun 2020 11:02:40 GMT -->
<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="Responsive Bootstrap 4 Dashboard and Admin Template">
    <meta name="author" content="ThemePixels">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<%=request.getContextPath() %>/resources/assets/img/favicon.png">

    <title>GestConge votre application de cong�</title>

    <!-- vendor css -->
    <link href="<%=request.getContextPath() %>/resources/lib/%40fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="<%=request.getContextPath() %>/resources/lib/ionicons/css/ionicons.min.css" rel="stylesheet">

    <!-- template css -->
    <link rel="stylesheet" href="<%=request.getContextPath() %>/resources/assets/css/cassie.css">

  </head>
  <body>

    <div class="signin-panel">
      <svg-to-inline path="../assets/svg/citywalk.svg" class-Name="svg-bg"></svg-to-inline>
      <img src="<%=request.getContextPath() %>/resources/assets/img/perso.jpg" class="img-fluid" alt="">
      
      <div class="signin-sidebar">
        <div class="signin-sidebar-body">
          <a href="dashboard-one.html" class="sidebar-logo mg-b-40"><span>GestConge</span></a>
          <h4 class="signin-title">Bienvenue � GestConge!</h4>
          <h5 class="signin-subtitle">Veuillez vous connecter pour continuer.</h5>

          <div class="signin-form">
            <div class="form-group">
              <label>Email</label>
              <input type="email" class="form-control" placeholder="Entrez votre Email" value="yourname@yourdomain.com">
            </div>

            <div class="form-group">
              <label class="d-flex justify-content-between">
                <span>Mot de Passe</span>
                <a href="#" class="tx-13">mot de passe oubli�?</a>
              </label>
              <input type="password" class="form-control" placeholder="Entrez votre mot de passe" value="yourpassword">
            </div>

            <div class="form-group d-flex mg-b-0">
              <button class="btn btn-brand-01 btn-uppercase flex-fill">Se connecter</button>
              <a href="page-signup.html" class="btn btn-white btn-uppercase flex-fill mg-l-10">S'inscrire</a>
            </div>

            <div class="divider-text mg-y-30">Or</div>

            <a href="dashboard-one.html" class="btn btn-facebook btn-uppercase btn-block">Se connecter avec Facebook</a>
          </div>
          <p class="mg-t-auto mg-b-0 tx-sm tx-color-03">En vous connectant, vous acceptez nos <a href="#">conditions d'utilisation</a>et notre <a href="#">politique de confidentialit�</a></p>
        </div><!-- signin-sidebar-body -->
      </div><!-- signin-sidebar -->
    </div><!-- signin-panel -->

    <script src="<%=request.getContextPath() %>/resources/lib/jquery/jquery.min.js"></script>
    <script src="<%=request.getContextPath() %>/resources/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<%=request.getContextPath() %>/resources/lib/feather-icons/feather.min.js"></script>
    <script src="<%=request.getContextPath() %>/resources/lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>

    <script>
      $(function(){

        'use strict'

        feather.replace();

        new PerfectScrollbar('.signin-sidebar', {
          suppressScrollX: true
        });

      })
    </script>
    <script src="<%=request.getContextPath() %>/resources/assets/js/svg-inline.js"></script>
  </body>

<!-- Mirrored from themepixels.me/cassie/pages/page-signin.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jun 2020 11:02:41 GMT -->
</html>
