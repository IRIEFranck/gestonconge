package com.irie.conge.dao;

import com.irie.conge.entites.Utilisateur;

public interface IUtilisateurDao  extends IGenericDao<Utilisateur> {

}
