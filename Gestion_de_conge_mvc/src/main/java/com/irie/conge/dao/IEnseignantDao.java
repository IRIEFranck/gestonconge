package com.irie.conge.dao;

import com.irie.conge.entites.Enseignant;

public interface IEnseignantDao extends IGenericDao<Enseignant> {

}
