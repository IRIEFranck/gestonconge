package com.irie.conge.dao;

import com.irie.conge.entites.Departement;

public interface IDepartementDao  extends IGenericDao<Departement> {

}
