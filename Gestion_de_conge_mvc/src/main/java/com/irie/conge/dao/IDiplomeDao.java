package com.irie.conge.dao;

import com.irie.conge.entites.Diplome;

public interface IDiplomeDao extends IGenericDao<Diplome> {

}