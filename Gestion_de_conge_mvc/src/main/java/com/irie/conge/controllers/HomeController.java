package com.irie.conge.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/home")
public class HomeController {
	
	
	@RequestMapping(value = "/")
	public String home() {
		
		return "home/home";
	}
	
	@RequestMapping(value = "/dashboard")
	public String dashboardHome() {
		
		return "dashboard/dashboard";
	}

}