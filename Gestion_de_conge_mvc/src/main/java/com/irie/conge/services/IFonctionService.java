package com.irie.conge.services;

import java.util.List;

import com.irie.conge.entites.Fonction;

public interface IFonctionService {
	
	public Fonction save(Fonction entity);
	public Fonction update(Fonction entity);
	public List<Fonction> selectAll();
	public List<Fonction> selectAll(String sortField, String sort);
	public Fonction getById(Long id);
 	public void  remove(Long id);
 	
    public Fonction findOne(String paramName, Object paramValue);
	
	public Fonction findOne(String[] paramNames, Object[] paramValues);
	
	public int findCountBy(String paramName, String paramValue);

}
