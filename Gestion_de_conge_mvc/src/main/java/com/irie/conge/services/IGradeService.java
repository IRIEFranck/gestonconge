package com.irie.conge.services;

import java.util.List;

import com.irie.conge.entites.Grade;

public interface IGradeService {
	
	
	public Grade save(Grade entity);
	public Grade update(Grade entity);
	public List<Grade> selectAll();
	public List<Grade> selectAll(String sortField, String sort);
	public Grade getById(Long id);
 	public void  remove(Long id);
 	
    public Grade findOne(String paramName, Object paramValue);
	
	public Grade findOne(String[] paramNames, Object[] paramValues);
	
	public int findCountBy(String paramName, String paramValue);

}
