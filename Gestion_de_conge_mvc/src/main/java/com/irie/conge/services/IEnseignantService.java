package com.irie.conge.services;

import java.util.List;

import com.irie.conge.entites.Enseignant;

public interface IEnseignantService {
	
	
	public Enseignant save(Enseignant entity);
	public Enseignant update(Enseignant entity);
	public List<Enseignant> selectAll();
	public List<Enseignant> selectAll(String sortField, String sort);
	public Enseignant getById(Long id);
 	public void  remove(Long id);
 	
    public Enseignant findOne(String paramName, Object paramValue);
	
	public Enseignant findOne(String[] paramNames, Object[] paramValues);
	
	public int findCountBy(String paramName, String paramValue);

}
