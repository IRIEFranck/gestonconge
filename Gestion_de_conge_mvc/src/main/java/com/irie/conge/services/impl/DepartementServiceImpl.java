
package com.irie.conge.services.impl;

import java.util.List;

import com.irie.conge.dao.IDepartementDao;
import com.irie.conge.entites.Departement;
import com.irie.conge.services.IDepartementService;

public class DepartementServiceImpl implements IDepartementService{

	
	private IDepartementDao dao;



	public void setDao(IDepartementDao dao) {
		this.dao = dao;
	}
	
	
	
	
	@Override
	public Departement save(Departement entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public Departement update(Departement entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public List<Departement> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<Departement> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Departement getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		dao.remove(id);
	}

	@Override
	public Departement findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Departement findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(paramName, paramValue);
	}
	
	
	
	

}
