package com.irie.conge.services.impl;

import java.util.List;


import org.springframework.transaction.annotation.Transactional;

import com.irie.conge.dao.IEnseignantDao;
import com.irie.conge.entites.Enseignant;
import com.irie.conge.services.IEnseignantService;


@Transactional
public class EnseignantServiceImpl implements IEnseignantService {

	
private IEnseignantDao dao;



public void setDao(IEnseignantDao dao) {
	this.dao = dao;
}

	@Override
	public Enseignant save(Enseignant entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public Enseignant update(Enseignant entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public List<Enseignant> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<Enseignant> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Enseignant getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		dao.remove(id);
	}

	@Override
	public Enseignant findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Enseignant findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(paramName, paramValue);
	}
 
}
