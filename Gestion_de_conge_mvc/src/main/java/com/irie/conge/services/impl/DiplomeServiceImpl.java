package com.irie.conge.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.irie.conge.dao.IDiplomeDao;
import com.irie.conge.entites.Diplome;
import com.irie.conge.services.IDiplomeService;


@Transactional
public class DiplomeServiceImpl implements IDiplomeService {
	
	
	
	private IDiplomeDao dao;



	public void setDao(IDiplomeDao dao) {
		this.dao = dao;
	}

	@Override
	public Diplome save(Diplome entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public Diplome update(Diplome entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public List<Diplome> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<Diplome> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Diplome getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		dao.remove(id);
	}

	@Override
	public Diplome findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Diplome findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(paramName, paramValue);
	}

}
