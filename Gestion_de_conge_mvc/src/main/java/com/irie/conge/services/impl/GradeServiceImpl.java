package com.irie.conge.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;


import com.irie.conge.dao.IGradeDao;
import com.irie.conge.entites.Grade;
import com.irie.conge.services.IGradeService;

@Transactional
public class GradeServiceImpl implements IGradeService {

	private IGradeDao dao;



	public void setDao(IGradeDao dao) {
		this.dao = dao;
	}
	
	
	
	
	@Override
	public Grade save(Grade entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public Grade update(Grade entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public List<Grade> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<Grade> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Grade getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		dao.remove(id);
	}

	@Override
	public Grade findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Grade findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(paramName, paramValue);
	}
	
	
	

}
