package com.irie.conge.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;


import com.irie.conge.dao.IFonctionDao;
import com.irie.conge.entites.Fonction;
import com.irie.conge.services.IFonctionService;


@Transactional
public class FonctionServiceImpl implements IFonctionService{

	
	private IFonctionDao dao;



	public void setDao(IFonctionDao dao) {
		this.dao = dao;
	}
	
	
	@Override
	public Fonction save(Fonction entity) {
		// TODO Auto-generated method stub
		return dao.save(entity);
	}

	@Override
	public Fonction update(Fonction entity) {
		// TODO Auto-generated method stub
		return dao.update(entity);
	}

	@Override
	public List<Fonction> selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	@Override
	public List<Fonction> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Fonction getById(Long id) {
		// TODO Auto-generated method stub
		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		dao.remove(id);
	}

	@Override
	public Fonction findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Fonction findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(paramName, paramValue);
	}
	

}
