package com.irie.conge.services;

import java.util.List;

import com.irie.conge.entites.Diplome;

public interface IDiplomeService {
	
	public Diplome save(Diplome entity);
	public Diplome update(Diplome entity);
	public List<Diplome> selectAll();
	public List<Diplome> selectAll(String sortField, String sort);
	public Diplome getById(Long id);
 	public void  remove(Long id);
 	
    public Diplome findOne(String paramName, Object paramValue);
	
	public Diplome findOne(String[] paramNames, Object[] paramValues);
	
	public int findCountBy(String paramName, String paramValue);

}
